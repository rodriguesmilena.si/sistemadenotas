import React, { Component } from 'react';
import './listaDeCategorias.css'

class ListaDeCategorias extends Component {

    _handleNovaCategoria(e){
        if(e.key === "Enter"){
            let nomeCategoria = e.target.value;
            this.props.criarCategoria(nomeCategoria);
        }
    }

    render() { 
        return ( 
            <section className="categorias-geral">
                <ul>
                    {this.props.categorias.map((categoria, index) => {
                        return (
                            <li key={index} className="item-categoria">
                                {categoria}
                            </li>
                        );
                      })} 
                </ul>
                <input type="text" 
                    className="lista-categoria-inpit"
                    placelhoder="Adiciar Categoria.."
                    onKeyUp={this._handleNovaCategoria.bind(this)} />
            </section>
           
         );
    }
} 
export default ListaDeCategorias;