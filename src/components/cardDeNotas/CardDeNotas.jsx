import React, { Component } from 'react';
import './cardDeNotas.css'
import {ReactComponent as DeleteSVG} from '../../assets/img/delete.svg'
class CardDeNotas extends Component {
    state = {  }

    apagar(){
      const id = this.props.id;
      this.props.apagarNota(id);
    }
    render() { 
        return (
            <section className="card-notas">
              <header className="card-nota-geral">
                <h4 className="card-nota-titulo">{this.props.titulo}</h4>
                <DeleteSVG onClick={this.apagar.bind(this)}/>
                <h5>{this.props.categoria}</h5>
              </header>
            <p>{this.props.descricao}</p>
          </section>
          );
    }
}
 
export default CardDeNotas;