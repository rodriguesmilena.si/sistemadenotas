import React, { Component } from 'react';
import './formularioDeNotas.css'

class FormularioDeNotas extends Component{

    constructor(props){
        super(props);
        this.titulo = "";
        this.descricao = "";
        this.categoria = "Sem Categoria";
    }

    _handleNovoTitulo(evento){
        evento.stopPropagation();       
        this.titulo = evento.target.value;
    }

    _handleNovaDescricao(evento){
        evento.stopPropagation();
        this.descricao = evento.target.value;
    }

    _handleNovaCategoria(evento){
        evento.stopPropagation();
        this.categoria = evento.target.value;
    }

    criarNota(evento){
        evento.preventDefault();
        evento.stopPropagation();
   
        this.props.criarNota(this.titulo, this.descricao, this.categoria);
    }

    render(){
        return(
            <form className="formulario" onSubmit={this.criarNota.bind(this)}>
                <p>Escreva aqui a sua nota...</p>
                <select onChange={this._handleNovaCategoria.bind(this)} className="input">
                    <option defaultChecked={true}>Sem Categoria</option>
                    {this.props.categorias.map(categoria => {
                        return(
                            <option> {categoria} </option>
                        )
                    })}
                </select>
                <input className="input" type="text" placeholder="Título" onChange={this._handleNovoTitulo.bind(this)}/>
                <textarea  rows={15} className="textArea" placeholder="Descrição.." onChange={this._handleNovaDescricao.bind(this)}/>
                <button className="botao">Criar uma nota</button>
          </form>
        )
        ;
    }
}

export default FormularioDeNotas;