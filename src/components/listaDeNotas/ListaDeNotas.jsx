import React, { Component } from "react";
import CardDeNotas from "../cardDeNotas/CardDeNotas";
import '../cardDeNotas/cardDeNotas.css'
import './listaDeNotas.css'

 class ListaDeNotas extends Component {
  render() {
    return (
      <div className="listaDeNotasGeral">
        <ul>
          {this.props.notas.map((nota, index) => {
            return ( 
              <li key={index} className="listaNotas">
                <CardDeNotas 
                id = {index}
                apagarNota = {this.props.apagarNota}
                titulo={nota.titulo} 
                descricao={nota.descricao}
                categoria={nota.categoria}/>
              </li>
            );
          })}
        
        </ul>
      </div>
    );
  }
}
export default ListaDeNotas;