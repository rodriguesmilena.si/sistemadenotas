import React, { Component } from 'react'
import FormularioDeNotas from './components/formularioDeNotas/FormularioDeNotas';
import ListaDeNotas from './components/listaDeNotas/ListaDeNotas'
import ListaDeCategorias from './components/listaDeCategorias/ListaDeCategorias'
import './assets/css/App.css'

class App extends Component {

  constructor() {
    super();
    this.state = {
      notas: [],
      categorias: [],
    }
  }

  criarNota(titulo, descricao, categoria) {
    const novaNota = { titulo, descricao, categoria }
    const novoArrayDeNotas = [...this.state.notas, novaNota]
    const novoState = {
      notas: novoArrayDeNotas
    }
    this.setState(novoState)
  }

  criarCategoria(nomeCategoria) {
    const novoArrayCategorias = [...this.state.categorias, nomeCategoria]
    const novoState = { ...this.state, categorias: novoArrayCategorias }
    this.setState(novoState);
  }

  deletarNota(id) {
    let arrayNotas = this.state.notas;
    arrayNotas.splice(id, 1);

    this.setState({ notas: arrayNotas });
  }

  render() {
    return (
      <div>
        <section className="conteudo">
          <FormularioDeNotas
            categorias={this.state.categorias}
            criarNota={this.criarNota.bind(this)} />
          <main className="conteudo-principal">
            <ListaDeCategorias
              criarCategoria={this.criarCategoria.bind(this)}
              categorias={this.state.categorias} />
            <ListaDeNotas
              apagarNota={this.deletarNota.bind(this)}
              notas={this.state.notas} />
          </main>
        </section>


      </div >
    );
  }
}

export default App;
